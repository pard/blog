+++
title = "Hello"
date = 2019-09-27
author = "pard68"
type = "post"
draft = false
+++

Hello!

This is my new theology blog. I hope to use it to work out my own theology, think through
various aspects of life and God, and to generally work on my sanctification and knowledge
of the Lord.
