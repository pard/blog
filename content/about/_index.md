+++
title =  "about"
template = "about.html"
+++

{{ image(path="/me.png" alt="Me!" class="centered" style="border-radius: 8px;" width=150, op="fit-width") }}

I'm Ian.

### Get in touch

 - <ian@hilasterion.com>
