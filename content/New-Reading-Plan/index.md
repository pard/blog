+++
title = "New Reading Plan"
date = 2020-08-16
author = "Ian S. Pringle"
type = "post"
draft = false
+++

So I've decided to amend Dr Horner's Bible Reading System after living with it
for a year. I had a few things I wanted to see different about it. First, I
wasn't pleased with the pacing of the OT. Due to how it's split up you just
don't see the whole of it enough (though technically you see 6 chapters of it a
day). In addition I wanted to get through the whole OT more than one and change
times per year and I wanted to see bigger chunks of what I was reading at a
time. I also found I was spending a lot of time flipping and consulting the
chart for what to read and not as much time in the reading.

So here is my plan for the next year, probably starting on Sept. 1st just for
ease of tracking:

 * Genesis - Malachi (skipping Psalms and Proverbs), 4 chapters per day
 * Psalms, a two chapters per day (morning and evening)
 * Proverbs, a chapter per day
 * Matthew - Acts, two chapters per day
 * Romans - Philemon, a chapter per day
 * Hebrews - Revelation, a chapter per day
 * Selected Book, a chapter per day

This will get me through the OT twice per year, the Gospels+Acts six times per
year, the Pauline Epistles four times per year, and the non-Pauline Epistles
six times per year. Additionally Proverbs will be read through 12 times per
year and a book I will select on a yearly basis will be read 365 / chapters in
selected book times per year.

I'm not attempting to read it this many times for the purpose of pride. Rather,
the Horner Bible Reading System's thesis is that there is great benefit in
reading a large amount of the Bible daily (11 chapters per day in mine,
probably an hour of reading) and seeing a bulk of the Bible in the year. His
system fails on seeing _whole_ Bible in a large quantity, as the way it is
divided up means some things you see very often and others you see only yearly.
His plan does attack the holistic portion of his plan's purpose better than
mine does, but I think this should still provide a good view amount of that.

The selected book is just a book that will be picked on a yearly basis and than
read through that book over and over throughout the year. This is based on
Horner's own system, where you read through the Book of Acts on a constant
basis. I think I will split the selected book reading up and do that at a
different time. Another aspect of Horner's plan is to just read the Bible.
Basically he believes that we got so bogged down in a few verses (whether
because we're into studying it or because it feels like enough) that we neglect
to consume the Bible voraciously. My hope is to balance the in-depth study with
the voracious consumption of the Bible by having a morning reading which will
be 10 chapters of the Bible (the voracious part) followed by a more methodical
and in-depth study of a single book over the course of 365 days. Ideally,
mental bandwidth and time allowing, this will include the memorizing of that
particular book (think I will start with my most favorite book, Galatians).
