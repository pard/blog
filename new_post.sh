#!/bin/bash

drafts_dir="$(pwd)/drafts/"
cd $drafts_dir

dashed=$(echo $1 | tr " " "-")
mkdir $dashed
cd $dashed
touch index.md
echo "+++" >> index.md
echo "title = \"$1\"" >> index.md
echo "date = $(date '+%Y-%m-%d')" >> index.md
echo "author = \"Ian S. Prinlge\"" >> index.md
echo "type = \"post\"" >> index.md
echo "draft = true" >> index.md
echo "+++" >> index.md
